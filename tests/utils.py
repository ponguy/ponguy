from contextlib import contextmanager
from unittest import mock

from ponguy.main import Runner
from ponguy.strip import START_HEADER_SIZE


def make_playing_game(opposite_player_sends=False):
    runner = Runner()

    pressed_button = runner.red_button if not opposite_player_sends else runner.blue_button

    game = runner.make_game(pressed_button)

    game.start_pingpong()

    return game


@contextmanager
def mock_button_press(player):
    def mock_value(self, on_off=None):
        if self.no == player.button.pin.no:
            return 0

    with mock.patch('machine.Pin.value', mock_value):
        yield


def get_pixel_color(game, i):
    index = i * 4 + START_HEADER_SIZE
    return tuple(reversed(game.strip._buf[index + 1 : index + 4]))


def get_range_color(game, start, end):
    color = get_pixel_color(game, start)

    if any(get_pixel_color(game, i) != color for i in range(start + 1, end)):
        raise ValueError('range contains multiple colors')

    return color
