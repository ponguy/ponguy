import colorsys
import json
import socket
import time
from unittest import mock

from ponguy import Colors, animation, bonus, config, preset
from ponguy.game import Game
from ponguy.main import Runner
from ponguy.remote_control import RemoteControl

from .utils import get_pixel_color, get_range_color, make_playing_game, mock_button_press


def test_full_game(freezer):
    runner = Runner()
    run = runner.run

    run()
    assert runner.game is None

    # red player starts game
    def mock_red_button_press(self, on_off=None):
        if self.no == config.RED_BUTTON_PIN:
            return 0

    with mock.patch('machine.Pin.value', mock_red_button_press):
        run()

    assert runner.game is None

    run()
    game = runner.game

    assert game.state == game.GETTING_READY
    assert game.sender.button.pin.no == config.RED_BUTTON_PIN

    # strip is animating
    run()
    assert game.state == game.GETTING_READY

    # skip round animation
    freezer.tick(game.strip.animation.duration_ms * 1000)
    run()
    assert game.strip.animation is None
    assert game.state == game.PLAYING

    assert game.balls[0].position == 0

    # ball won't move until some time
    run()
    assert game.balls[0].position == 0

    # hitting zones are displayed
    assert get_pixel_color(game, 0) == game.sender.color
    assert get_pixel_color(game, config.HITTING_RANGE - 1) == game.sender.color
    assert get_range_color(game, 1, config.HITTING_RANGE - 1) == Colors.BLACK

    assert get_pixel_color(game, game.strip.num_leds - 1) == game.receiver.color
    assert get_pixel_color(game, game.strip.num_leds - config.HITTING_RANGE) == game.receiver.color
    assert get_range_color(game, game.strip.num_leds - config.HITTING_RANGE + 1, game.strip.num_leds - 1) == (
        0,
        0,
        0,
    )

    # accelerate time until ball moves
    freezer.tick(game.speed)
    run()
    assert game.balls[0].position == 1

    run()
    assert get_pixel_color(game, 1) == game.sender.color
    assert get_pixel_color(game, 2) == Colors.BLACK

    for i in range(2, 145):
        freezer.tick(game.speed)
        run()
        assert game.balls[0].position == i

        run()
        ball_last_pixel_index = max(0, i - config.BALL_LENGTH + 1)

        # check ball end, ignoring hitting zone dots
        if ball_last_pixel_index not in (1, config.HITTING_RANGE):
            assert get_pixel_color(game, ball_last_pixel_index - 1) == Colors.BLACK

        # check ball body
        assert get_pixel_color(game, i) == game.sender.color
        for j in range(ball_last_pixel_index, i):
            assert get_pixel_color(game, j)[0] < 255

        # check ball start, ignoring hitting zone dots
        if i not in (
            config.HITTING_RANGE - 2,
            game.receiver.position,
            game.receiver.position - config.HITTING_RANGE,
        ):
            assert get_pixel_color(game, i + 1) == Colors.BLACK

    # blue player sends back
    def mock_blue_button_press(self, on_off=None):
        if self.no == config.BLUE_BUTTON_PIN:
            return 0

    with mock.patch('machine.Pin.value', mock_blue_button_press):
        run()
    run()

    freezer.tick(game.speed)
    run()
    assert game.balls[0].position == 143

    run()
    assert get_pixel_color(game, 143) == game.receiver.color == game.balls[0].sender.color

    for i in range(142, -1, -1):
        freezer.tick(game.speed)
        run()
        assert game.balls[0].position == i
        run()
        assert get_pixel_color(game, i) == game.receiver.color
        assert get_pixel_color(game, i + 1)[2] < 255

    freezer.tick(game.speed)
    run()
    assert game.state == game.GETTING_READY

    # skip round animation
    freezer.tick(game.strip.animation.duration_ms * 1000)
    run()
    assert game.strip.animation is None
    assert game.state == game.PLAYING

    # lose ball
    freezer.tick(game.speed * 150)
    run()
    assert game.state == game.GETTING_READY

    # skip round animation
    freezer.tick(game.strip.animation.duration_ms * 1000)
    run()
    assert game.strip.animation is None
    assert game.state == game.PLAYING

    # lose ball
    freezer.tick(game.speed * 150)
    run()
    assert game.state == game.ENDING

    # test round animation
    for i in range(1000):
        freezer.tick(1000)
        run()


def test_missed_ball_fading(freezer):
    game = make_playing_game()
    game.bonus_classes = []
    ball = game.balls[0]
    game.pingpong()

    # move ball to just before opponent hitting zone
    target_time = game.speed * (ball.receiver.first_hit_position - ball.position - 1)
    freezer.move_to(time.ticks_us() + target_time)
    game.pingpong()
    game.pingpong()

    # ball is displayed
    assert ball.position == game.strip.num_leds - config.HITTING_RANGE - 1
    assert get_pixel_color(game, ball.position) == ball.sender.color
    # just before hitting zone dot
    assert get_pixel_color(game, ball.position + 1) == ball.receiver.color

    ball_position = ball.position

    # receiver presses too soon
    with mock_button_press(ball.receiver):
        game.pingpong()

    assert ball.speed == 0
    assert ball.position == ball_position

    ball_color_head = get_pixel_color(game, ball.position)
    ball_color_tail = get_pixel_color(game, ball.position - ball.length + 1)
    assert colorsys.rgb_to_hsv(*ball_color_head) == (0.0, 1.0, 255)
    assert colorsys.rgb_to_hsv(*ball_color_tail) == (0.0, 1.0, 50)

    freezer.tick(50000)
    game.pingpong()

    assert ball.speed == 0
    assert ball.position == ball_position

    ball_color_head = get_pixel_color(game, ball.position)
    ball_color_tail = get_pixel_color(game, ball.position - ball.length + 1)
    assert colorsys.rgb_to_hsv(*ball_color_head) == (0.0, 1.0, 242)
    assert colorsys.rgb_to_hsv(*ball_color_tail) == (0.0, 1.0, 47)

    freezer.tick(50000)
    game.pingpong()

    ball_color_head = get_pixel_color(game, ball.position)
    ball_color_tail = get_pixel_color(game, ball.position - ball.length + 1)
    assert colorsys.rgb_to_hsv(*ball_color_head) == (0.0, 1.0, 229)
    assert colorsys.rgb_to_hsv(*ball_color_tail) == (0.0, 1.0, 44)

    freezer.tick(1000000)
    game.pingpong()
    assert game.balls == []


def test_scoreboad_api(freezer, local_config):
    local_config.API_CREATE_URL = 'http://scoreboard.org/ponguy/'
    local_config.API_EDIT_URL = 'http://scoreboard.org/ponguy/%s/'
    local_config.API_USER = 'user'
    local_config.API_SECRET = 'pass'

    class MockedRequestResponse(mock.Mock):
        status_code = 200

        def json(self):
            return json.loads(self.content)

    with mock.patch('ponguy.utils.urequests.post') as urequests_get:
        response_payload = {'data': {'id': 42}}
        urequests_get.return_value = MockedRequestResponse(content=json.dumps(response_payload))

        game = make_playing_game()

        assert urequests_get.call_count == 1
        assert urequests_get.call_args.kwargs['json'] == {'data': {}}

    for player in game.players:
        player.life_points = 1

    ball = game.balls[0]
    game.pingpong()

    # first send back
    freezer.skip_to_position(ball, ball.receiver.first_hit_position + 1)

    with mock_button_press(ball.receiver):
        game.pingpong()

    freezer.skip_to_position(ball, ball.receiver.first_hit_position)

    with mock_button_press(ball.receiver):
        game.pingpong()

    # loose ball
    freezer.tick(game.speed * 150)

    with mock.patch('ponguy.utils.urequests.post') as urequests_get:
        game.pingpong()

        assert urequests_get.call_count == 1
        assert urequests_get.call_args.args[0] == 'http://scoreboard.org/ponguy/42/'
        assert urequests_get.call_args.kwargs['json'] == {
            'data': {'duration': 2, 'win_color': 'red', 'ball_count': 2}
        }

    # specify game slug
    local_config.API_GAME_SLUG = 'new-year-competition'
    with mock.patch('ponguy.utils.urequests.post') as urequests_get:
        response_payload = {'data': {'id': 42}}
        urequests_get.return_value = MockedRequestResponse(content=json.dumps(response_payload))

        game = make_playing_game()

        assert urequests_get.call_count == 1
        assert urequests_get.call_args.kwargs['json'] == {'data': {'game_slug': 'new-year-competition'}}

    # request error
    with mock.patch('ponguy.utils.urequests.post') as urequests_get:
        urequests_get.side_effect = ValueError

        game = make_playing_game()

        assert urequests_get.call_count == 1
        assert game.remote_game_id is None

    # status not 200
    with mock.patch('ponguy.utils.urequests.post') as urequests_get:
        urequests_get.return_value = MockedRequestResponse(status_code=404)

        game = make_playing_game()

        assert urequests_get.call_count == 1
        assert game.remote_game_id is None

    # response is invalid json
    with mock.patch('ponguy.utils.urequests.post') as urequests_get:
        urequests_get.return_value = MockedRequestResponse(content=b'{')

        game = make_playing_game()

        assert urequests_get.call_count == 1
        assert game.remote_game_id is None

    # response payload indicates error
    with mock.patch('ponguy.utils.urequests.post') as urequests_get:
        response_payload = {'data': {'id': 42}, 'err': 1}
        urequests_get.return_value = MockedRequestResponse(content=json.dumps(response_payload))

        game = make_playing_game()

        assert urequests_get.call_count == 1
        assert game.remote_game_id is None

    # response payload has not data key
    with mock.patch('ponguy.utils.urequests.post') as urequests_get:
        response_payload = {'xxx': {'id': 42}}
        urequests_get.return_value = MockedRequestResponse(content=json.dumps(response_payload))

        game = make_playing_game()

        assert urequests_get.call_count == 1
        assert game.remote_game_id is None


def test_game_presets(freezer, local_config):
    # nothing happens when presets are disabled
    local_config.GAME_PRESETS_PROBABILITY = 0

    game = make_playing_game()
    assert game.round_animation_class is animation.RoundAnimation
    assert game.bonus_classes == Game.bonus_classes
    assert game.speed == Game.speed

    # force preset use on every game
    local_config.GAME_PRESETS_PROBABILITY = 1

    # no bonus preset
    with mock.patch('ponguy.game.get_random_game_preset', return_value=preset.GamePresetNoBonus()):
        game = make_playing_game()
    game.pingpong()

    assert game.round_animation_class is animation.RoundPresetAnimation
    assert game.bonus_classes == []
    assert game.speed == int(Game.speed * 0.8)

    # multiball preset
    with mock.patch('ponguy.game.get_random_game_preset', return_value=preset.GamePresetOnlyMultiBall()):
        game = make_playing_game()
    game.pingpong()

    assert game.bonus_classes == [bonus.BallAddBonus]
    assert all(x.life_points == 5 for x in game.players)

    # inversion preset
    with mock.patch('ponguy.game.get_random_game_preset', return_value=preset.GamePresetOnlyInversion()):
        game = make_playing_game()
    game.pingpong()

    assert game.bonus_classes == [bonus.ReverseBonus]

    # breakout and blink preset
    with mock.patch(
        'ponguy.game.get_random_game_preset', return_value=preset.GamePresetOnlyBreakoutAndBlink()
    ):
        game = make_playing_game()
    game.pingpong()

    assert game.bonus_classes == [bonus.BreakoutBonus, bonus.BlinkingBonus]

    # breakout and rainbow preset
    with mock.patch(
        'ponguy.game.get_random_game_preset', return_value=preset.GamePresetOnlyBreakoutAndRainbow()
    ):
        game = make_playing_game()
    game.pingpong()

    assert game.bonus_classes == [bonus.BreakoutBonus, bonus.RainbowBonus]


@mock.patch('ponguy.settings_holder.open', new_callable=mock.mock_open)
def test_game_remote_control(mocked_open, freezer, monkeypatch):
    mocked_open().read.return_value = '{"delay": 6000}'

    monkeypatch.setattr(config, 'mutable_settings', ['DELAY'])
    config.__init__()

    runner = Runner()

    poller = mock.MagicMock()
    remote_control_socket = mock.Mock(spec=socket.socket)
    with mock.patch('socket.socket', return_value=remote_control_socket):
        with mock.patch('select.poll', return_value=poller):
            runner.remote_control = RemoteControl()

    runner.run()

    # red player starts game
    def mock_red_button_press(self, on_off=None):
        if self.no == config.RED_BUTTON_PIN:
            return 0

    with mock.patch('machine.Pin.value', mock_red_button_press):
        runner.run()
    runner.run()
    game = runner.game

    # strip is animating
    runner.run()
    assert game.state == game.GETTING_READY

    # skip round animation
    freezer.tick(game.strip.animation.duration_ms * 1000)
    runner.run()
    runner.run()

    poller.poll.return_value = [(remote_control_socket, 42)]

    recv_socket = mock.Mock()
    remote_control_socket.accept.return_value = (recv_socket, 42)

    request_body = b'GET / HTTP/1.1\r\nHost: localhost:42424\r\n\r\n'
    recv_socket.recv.return_value = request_body

    runner.run()
    assert [x.args[0] for x in recv_socket.write.call_args_list] == [
        'HTTP/1.0 200 OK\r\n',
        'Content-type: application/json\r\n',
        'Access-Control-Allow-Origin: *\r\n',
        'Connection: close\r\n\r\n',
        '{"delay": 6000}',
    ]

    request_body = (
        b'POST / HTTP/1.1\r\nHost: localhost:8000\r\nAccept: */*\r\n'
        b'Content-Type: application/json\r\nContent-Length: 17\r\n\r\n'
        b'{"delay": "1000"}'
    )
    recv_socket.reset_mock()
    recv_socket.recv.return_value = request_body

    runner.run()

    assert game.speed == 1000
    assert [x.args[0] for x in recv_socket.write.call_args_list] == [
        'HTTP/1.0 200 OK\r\n',
        'Content-type: application/json\r\n',
        'Access-Control-Allow-Origin: *\r\n',
        'Connection: close\r\n\r\n',
        '{}',
    ]
    mocked_open().write.assert_called_once_with('{"delay": 1000}')

    config.mutable_settings.append('SPEEDING_BONUS_PROBABILITY')
    game.bonus_classes = [bonus.SpeedingBonus]
    game.scheduled_bonus_micros = time.ticks_us() + game.speed * 3

    freezer.move_to(game.scheduled_bonus_micros)
    runner.run()

    assert len(game.displayed_bonuses) == 1
    assert game.displayed_bonuses[0].probability == 55

    request_body = (
        b'POST / HTTP/1.1\r\nHost: localhost:8000\r\nAccept: */*\r\n'
        b'Content-Type: application/json\r\nContent-Length: 15\r\n\r\n'
        b'{"speeding_bonus_probability": "42"}'
    )
    recv_socket.reset_mock()
    recv_socket.recv.return_value = request_body

    runner.run()

    assert bonus.SpeedingBonus.probability == 42
    assert game.displayed_bonuses[0].probability == 42

    # receive request split into multiple fragments
    recv_socket.reset_mock()
    recv_socket.recv.side_effect = [
        b'POST / HTTP/1.1\r\nHost: localhost:8000\r\nAccept: */*\r\n',
        b'Content-Type: application/json\r\nContent-Length: 17\r\n\r\n',
        b'{"delay": "3000"}',
    ]

    runner.run()
    assert game.speed == 3000
    assert recv_socket.recv.call_count == 3

    # error, invalid json
    request_body = (
        b'POST / HTTP/1.1\r\nHost: localhost:8000\r\nAccept: */*\r\n'
        b'Content-Type: application/json\r\nContent-Length: 17\r\n\r\n'
        b'{"delay": "1000"'
    )
    recv_socket.reset_mock()
    recv_socket.recv.side_effect = None
    recv_socket.recv.return_value = request_body

    runner.run()
    assert game.speed == 3000

    # error on receive
    recv_socket.reset_mock()
    recv_socket.recv.side_effect = OSError

    runner.run()
    assert game.speed == 3000

    recv_socket.reset_mock()
    recv_socket.recv.side_effect = [
        b'POST / HTTP/1.1\r\nHost: localhost:8000\r\nAccept: */*\r\n',
        b'Content-Type: application/json\r\nContent-Length: 17\r\n\r\n',
        OSError,
    ]

    runner.run()
    assert game.speed == 3000
