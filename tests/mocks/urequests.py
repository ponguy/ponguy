# methods in this module should always be mocked explicitly


def post(*args, **kwargs):
    raise NotImplementedError
