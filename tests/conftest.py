import _thread
import sys

import pytest

from .mocks import machine, micropython, network, time, ucontextlib, urequests

sys.modules['machine'] = machine
sys.modules['micropython'] = micropython
sys.modules['time'] = time
sys.modules['ucontextlib'] = ucontextlib
sys.modules['urequests'] = urequests
sys.modules['network'] = network
sys.modules['ponguy.config_override'] = None


@pytest.fixture(autouse=True)
def disable_threads(monkeypatch):
    def start_new_thread(func, args_tuple):
        return func(*args_tuple)

    monkeypatch.setattr(_thread, 'start_new_thread', start_new_thread)


@pytest.fixture
def freezer(monkeypatch):
    original_ticks_us = time.ticks_us

    class Freezer:
        microseconds = None

        @classmethod
        def ticks_ms(cls):
            return cls.get_microseconds() // 10**3

        @classmethod
        def ticks_us(cls):
            return cls.get_microseconds()

        @classmethod
        def get_microseconds(cls):
            return cls.microseconds or original_ticks_us()

        @classmethod
        def move_to(cls, microseconds):
            cls.microseconds = microseconds

        @classmethod
        def tick(cls, microseconds):
            cls.microseconds = cls.get_microseconds() + microseconds

        @classmethod
        def skip_to_position(cls, ball, position):
            if ball.sender.direction == 1:
                assert ball.position < position
                steps = position - ball.position
            else:
                assert ball.position > position
                steps = ball.position - position

            target_time = ball.game.speed * steps
            cls.tick(target_time)
            ball.game.pingpong()
            assert ball.position == position

    monkeypatch.setattr(time, 'ticks_ms', Freezer.ticks_ms)
    monkeypatch.setattr(time, 'ticks_us', Freezer.ticks_us)

    return Freezer


@pytest.fixture
def local_config(monkeypatch):
    from ponguy import config

    class LocalConfig:
        def __setattr__(self, name, value):
            monkeypatch.setattr(config, name, value)

    yield LocalConfig()
