from unittest import mock

from ponguy.animation import RandomColors, RastaFlag
from ponguy.strip import Strip

from .utils import get_pixel_color


def test_animation_done(freezer):
    strip = Strip()
    animation = RandomColors(strip)
    animation.duration_ms = 10
    animation.init_anim()

    assert animation.done() is False

    freezer.tick(5 * 1000)
    assert animation.done() is False

    freezer.tick(6 * 1000)
    assert animation.done() is True


def test_rasta_flag(freezer, local_config):
    local_config.NUM_LEDS = 6

    strip = Strip()
    game = mock.Mock(strip=strip)
    animation = RastaFlag(strip)
    animation.color_groups = 2

    animation.init_anim()
    animation.play()
    assert [get_pixel_color(game, i) for i in range(6)] == [
        (0, 255, 0),
        (0, 255, 0),
        (0, 70, 0),
        (0, 70, 0),
        (47, 23, 0),
        (47, 23, 0),
    ]
    animation.play()
    assert [get_pixel_color(game, i) for i in range(6)] == [
        (0, 70, 0),
        (0, 70, 0),
        (0, 70, 0),
        (0, 70, 0),
        (171, 85, 0),
        (171, 85, 0),
    ]
