import time

from ponguy import Colors
from ponguy.bonus import (
    BallAddBonus,
    BlinkingBonus,
    BreakoutBonus,
    LifeAddBonus,
    RainbowBonus,
    ReverseBonus,
    SpeedingBonus,
    VolleyBonus,
)

from .utils import get_pixel_color, get_range_color, make_playing_game, mock_button_press


def test_bonus_general_mechanism(freezer):
    game = make_playing_game()
    game.bonus_classes = [LifeAddBonus]
    game.scheduled_bonus_micros = time.ticks_us() + game.speed * 3

    game.pingpong()
    assert game.displayed_bonuses == []

    freezer.move_to(game.scheduled_bonus_micros)
    game.pingpong()

    life_add_bonus = game.displayed_bonuses[0]
    assert life_add_bonus.coord > game.balls[0].position

    assert get_pixel_color(game, life_add_bonus.coord - 1) == Colors.BLACK
    assert (
        get_range_color(game, life_add_bonus.coord, life_add_bonus.coord + life_add_bonus.length)
        == life_add_bonus.color
    )
    assert get_pixel_color(game, life_add_bonus.coord + life_add_bonus.length) == Colors.BLACK

    ball = game.balls[0]
    assert not ball.bonus
    assert ball.sender.life_points == 3

    # move ball on bonus
    freezer.skip_to_position(ball, life_add_bonus.coord)

    # opponent tries to take bonus
    with mock_button_press(ball.receiver):
        game.pingpong()

    # nothing happens
    assert life_add_bonus in game.displayed_bonuses

    # sender takes bonus
    with mock_button_press(ball.sender):
        game.pingpong()

    assert ball.bonus == life_add_bonus
    assert not ball.sender.bonus
    assert ball.sender.life_points == 4
    assert life_add_bonus not in game.displayed_bonuses

    game.pingpong()
    assert get_pixel_color(game, ball.position) == life_add_bonus.color


def test_speeding_bonus(freezer):
    game = make_playing_game()
    ball = game.balls[0]
    game.bonus_classes = [SpeedingBonus]
    game.scheduled_bonus_micros = time.ticks_us() + game.speed * 3

    freezer.move_to(game.scheduled_bonus_micros)
    game.pingpong()
    speeding_bonus = game.displayed_bonuses[0]

    # move ball on bonus
    freezer.skip_to_position(ball, speeding_bonus.coord)

    # take bonus
    with mock_button_press(ball.sender):
        game.pingpong()

    assert speeding_bonus not in game.displayed_bonuses
    assert not ball.bonus
    assert ball.sender.bonus == speeding_bonus

    # hitting zone dot color changed
    game.pingpong()
    assert get_pixel_color(game, ball.sender.position) == speeding_bonus.color

    # move ball to opponent hitting zone
    freezer.skip_to_position(ball, ball.receiver.first_hit_position + 1)

    # opponent sends back
    with mock_button_press(ball.receiver):
        game.pingpong()

    # move ball to player hitting zone
    freezer.skip_to_position(ball, ball.receiver.first_hit_position - 1)

    # player sends back
    with mock_button_press(ball.receiver):
        game.pingpong()

    assert speeding_bonus not in game.displayed_bonuses
    assert ball.bonus == speeding_bonus
    assert not ball.sender.bonus

    for _ in range(100):
        freezer.tick(game.speed)
        game.pingpong()

    assert ball.position == 138  # ball has moved faster


def test_breakout_bonus(freezer, local_config):
    game = make_playing_game()
    ball = game.balls[0]
    game.speed = BreakoutBonus.blinking_delay * 10
    game.bonus_classes = [BreakoutBonus]
    game.scheduled_bonus_micros = time.ticks_us() + game.speed * 3

    freezer.move_to(game.scheduled_bonus_micros)
    game.pingpong()
    breakout_bonus = game.displayed_bonuses[0]

    # check blink
    assert get_pixel_color(game, breakout_bonus.coord) == breakout_bonus.color

    freezer.tick(breakout_bonus.blinking_delay)
    game.pingpong()
    assert get_pixel_color(game, breakout_bonus.coord) == Colors.BLACK

    freezer.tick(breakout_bonus.blinking_delay)
    game.pingpong()
    assert get_pixel_color(game, breakout_bonus.coord) == breakout_bonus.color

    # move ball on bonus
    freezer.skip_to_position(ball, breakout_bonus.coord)

    # activate bonus
    with mock_button_press(ball.sender):
        game.pingpong()

    assert breakout_bonus not in game.displayed_bonuses
    assert breakout_bonus in game.active_bonuses
    assert not ball.bonus
    assert not ball.sender.bonus

    # check no blink
    assert get_pixel_color(game, breakout_bonus.coord) == breakout_bonus.color

    freezer.tick(breakout_bonus.blinking_delay)
    game.pingpong()
    assert get_pixel_color(game, breakout_bonus.coord) == breakout_bonus.color

    # move ball to opponent hitting zone
    freezer.skip_to_position(ball, ball.receiver.first_hit_position + 1)

    # opponent sends back
    with mock_button_press(ball.receiver):
        game.pingpong()

    # move ball to just before breakout bonus
    freezer.skip_to_position(ball, breakout_bonus.coord + breakout_bonus.length + 2)
    assert breakout_bonus in game.active_bonuses
    assert ball.sender.direction == -1

    for _ in range(10):
        freezer.tick(game.speed)
        game.pingpong()

    assert breakout_bonus not in game.active_bonuses
    assert ball.sender.direction == 1  # ball is going back


def test_breakout_bonus_blinking_bypass(freezer):
    game = make_playing_game()
    game.bonus_classes = []
    ball = game.balls[0]
    game.pingpong()

    breakout_bonus = BreakoutBonus(game, game.strip, 50)
    game.displayed_bonuses.append(breakout_bonus)

    blinking_bonus = BlinkingBonus(game, game.strip, 100)
    game.displayed_bonuses.append(blinking_bonus)

    # move ball on breakout bonus
    freezer.skip_to_position(ball, breakout_bonus.coord)

    # activate bonus
    with mock_button_press(ball.sender):
        game.pingpong()

    assert breakout_bonus not in game.displayed_bonuses
    assert breakout_bonus in game.active_bonuses
    assert breakout_bonus.blocking is True

    # move ball to opponent hitting zone
    freezer.skip_to_position(ball, ball.receiver.first_hit_position + 1)

    # opponent sends back
    with mock_button_press(ball.receiver):
        game.pingpong()

    # move ball on blinking bonus
    freezer.skip_to_position(ball, blinking_bonus.coord)

    # activate bonus
    with mock_button_press(ball.sender):
        game.pingpong()

    assert blinking_bonus not in game.displayed_bonuses
    assert ball.bonus == blinking_bonus

    # move ball to just before breakout bonus
    freezer.skip_to_position(ball, breakout_bonus.coord + breakout_bonus.length + 2)
    assert breakout_bonus in game.active_bonuses
    assert breakout_bonus.blocking is True
    assert ball.sender.direction == -1

    for _ in range(10):
        freezer.tick(game.speed)
        game.pingpong()

    assert breakout_bonus in game.active_bonuses
    assert breakout_bonus.blocking is True
    assert ball.sender.direction == -1  # ball is still going in the same direction


def test_ball_add_bonus(freezer):
    game = make_playing_game()
    ball = game.balls[0]
    game.bonus_classes = [BallAddBonus]
    game.scheduled_bonus_micros = time.ticks_us() + game.speed * 3

    freezer.move_to(game.scheduled_bonus_micros)
    game.pingpong()
    ball_add_bonus = game.displayed_bonuses[0]

    # move ball on bonus
    freezer.skip_to_position(ball, ball_add_bonus.coord)

    # take bonus
    with mock_button_press(ball.sender):
        game.pingpong()

    assert not ball.sender.bonus
    assert not ball.bonus
    assert ball_add_bonus not in game.displayed_bonuses

    first_ball_position = game.balls[0].position
    second_ball_position = game.balls[1].position

    for _ in range(10):
        freezer.tick(game.speed)
        game.pingpong()

    # two balls are now moving
    assert game.balls[0].position > first_ball_position
    assert game.balls[1].position > second_ball_position


def test_rainbow_bonus(freezer):
    game = make_playing_game()
    ball = game.balls[0]
    game.bonus_classes = [RainbowBonus]
    game.scheduled_bonus_micros = time.ticks_us() + game.speed * 3

    freezer.move_to(game.scheduled_bonus_micros)
    game.pingpong()
    rainbow_bonus = game.displayed_bonuses[0]

    # move ball on bonus
    freezer.skip_to_position(ball, rainbow_bonus.coord)

    # take bonus
    with mock_button_press(ball.sender):
        game.pingpong()

    assert rainbow_bonus not in game.displayed_bonuses
    assert not ball.bonus
    assert ball.sender.bonus == rainbow_bonus

    # move ball to opponent hitting zone
    freezer.skip_to_position(ball, ball.receiver.first_hit_position + 1)

    # opponent sends back
    with mock_button_press(ball.receiver):
        game.pingpong()

    # move ball to just before player hitting zone
    freezer.skip_to_position(ball, ball.receiver.first_hit_position + 1)

    game.pingpong()
    assert ball.sender.direction == -1

    for _ in range(10):
        freezer.tick(game.speed)
        game.pingpong()

    # rainbow starts with player color
    assert get_pixel_color(game, ball.sender.first_hit_position) == (255, 0, 0)
    assert get_pixel_color(game, ball.sender.first_hit_position + 1) == (237, 18, 0)
    assert get_pixel_color(game, ball.sender.first_hit_position + 2) == (218, 37, 0)

    assert ball.sender.direction == 1  # ball is going back automatically
    assert ball.bonus.__class__ == RainbowBonus
    assert not ball.sender.bonus

    for _ in range(50):
        freezer.tick(game.speed)
        game.pingpong()

    assert ball.position == 127  # ball is moving fast

    # rainbow is stable
    assert get_pixel_color(game, ball.sender.first_hit_position) == (255, 0, 0)
    assert get_pixel_color(game, ball.sender.first_hit_position + 1) == (237, 18, 0)
    assert get_pixel_color(game, ball.sender.first_hit_position + 2) == (218, 37, 0)


def test_rainbow_bonus_reverse(freezer):
    game = make_playing_game(opposite_player_sends=True)
    ball = game.balls[0]
    game.bonus_classes = [RainbowBonus]
    game.scheduled_bonus_micros = time.ticks_us() + game.speed * 3

    freezer.move_to(game.scheduled_bonus_micros)
    game.pingpong()
    rainbow_bonus = game.displayed_bonuses[0]

    # move ball on bonus
    freezer.skip_to_position(ball, rainbow_bonus.coord)

    # take bonus
    with mock_button_press(ball.sender):
        game.pingpong()

    # move ball to opponent hitting zone
    freezer.skip_to_position(ball, ball.receiver.first_hit_position - 1)

    # opponent sends back
    with mock_button_press(ball.receiver):
        game.pingpong()

    # move ball to just before player hitting zone
    freezer.skip_to_position(ball, ball.receiver.first_hit_position - 1)

    game.pingpong()
    assert ball.sender.direction == 1

    for _ in range(10):
        freezer.tick(game.speed)
        game.pingpong()

    # rainbow starts with player color
    assert get_pixel_color(game, ball.sender.first_hit_position) == (0, 0, 255)
    assert get_pixel_color(game, ball.sender.first_hit_position - 1) == (0, 38, 218)
    assert get_pixel_color(game, ball.sender.first_hit_position - 2) == (0, 75, 181)

    assert ball.sender.direction == -1  # ball is going back automatically

    freezer.tick(game.speed * 2)
    game.pingpong()

    # rainbow is stable
    assert get_pixel_color(game, ball.sender.first_hit_position) == (0, 0, 255)
    assert get_pixel_color(game, ball.sender.first_hit_position - 1) == (0, 38, 218)
    assert get_pixel_color(game, ball.sender.first_hit_position - 2) == (0, 75, 181)


def test_bonus_probabilities(freezer):
    class FirstBonus(LifeAddBonus):
        pass

    class SecondBonus(LifeAddBonus):
        pass

    class ThirdBonus(LifeAddBonus):
        pass

    game = make_playing_game()
    game.bonus_classes = [FirstBonus, SecondBonus, ThirdBonus]

    for bonus in game.bonus_classes:
        bonus.probability = 0

    game.scheduled_bonus_micros = time.ticks_us() + game.speed * 3

    game.pingpong()
    assert game.displayed_bonuses == []

    freezer.move_to(game.scheduled_bonus_micros)
    game.pingpong()
    assert game.displayed_bonuses == []

    SecondBonus.probability = 1
    game.pingpong()
    assert len(game.displayed_bonuses) == 1
    assert game.displayed_bonuses[0].__class__ == SecondBonus


def test_reverse_bonus(freezer):
    game = make_playing_game()
    ball = game.balls[0]

    ReverseBonus.probability = 1
    game.bonus_classes = [ReverseBonus]
    game.scheduled_bonus_micros = time.ticks_us() + game.speed * 3

    freezer.move_to(game.scheduled_bonus_micros)
    game.pingpong()
    reverse_bonus = game.displayed_bonuses[0]

    # move ball on bonus
    freezer.skip_to_position(ball, reverse_bonus.coord)

    # take bonus
    with mock_button_press(ball.sender):
        game.pingpong()

    assert not ball.sender.bonus
    assert not ball.bonus
    assert reverse_bonus not in game.displayed_bonuses
    assert game.strip.reversed_display is True

    game.pingpong()  # cover Strip.reverse_buffer()

    # lose ball
    freezer.tick(game.speed * 150)
    game.pingpong()
    assert game.strip.reversed_display is False


def test_volley_bonus(freezer):
    game = make_playing_game()
    ball = game.balls[0]

    VolleyBonus.probability = 1
    game.bonus_classes = [VolleyBonus]
    game.scheduled_bonus_micros = time.ticks_us() + game.speed * 3

    freezer.move_to(game.scheduled_bonus_micros)
    game.pingpong()
    volley_bonus = game.displayed_bonuses[0]

    assert get_pixel_color(game, volley_bonus.coord) == volley_bonus.color
    assert get_range_color(game, volley_bonus.coord + 1, volley_bonus.coord + volley_bonus.length - 1) == (
        20,
        9,
        0,
    )
    assert get_pixel_color(game, volley_bonus.coord + volley_bonus.length - 1) == volley_bonus.color

    # move ball on bonus
    freezer.skip_to_position(ball, volley_bonus.coord)

    # take bonus
    with mock_button_press(ball.sender):
        game.pingpong()

    assert get_pixel_color(game, volley_bonus.coord) == volley_bonus.color
    assert (
        get_range_color(game, volley_bonus.coord + 1, volley_bonus.coord + volley_bonus.length - 1)
        == Colors.BLACK
    )
    assert get_pixel_color(game, volley_bonus.coord + volley_bonus.length - 1) == volley_bonus.color

    assert ball.sender.bonus
    assert not ball.bonus
    assert volley_bonus not in game.displayed_bonuses
    assert volley_bonus in game.active_bonuses

    # move ball to opponent hitting zone
    freezer.skip_to_position(ball, ball.receiver.first_hit_position + 1)

    # opponent sends back
    with mock_button_press(ball.receiver):
        game.pingpong()

    assert get_pixel_color(game, volley_bonus.coord) == volley_bonus.color
    assert (
        get_range_color(game, volley_bonus.coord + 1, volley_bonus.coord + volley_bonus.length - 1)
        == Colors.BLACK
    )
    assert get_pixel_color(game, volley_bonus.coord + volley_bonus.length - 1) == volley_bonus.color

    # move ball to bonus
    freezer.skip_to_position(ball, volley_bonus.coord + volley_bonus.length / 2)
    assert ball.sender.direction == -1

    # first player sends back with volley bonus
    with mock_button_press(ball.receiver):
        game.pingpong()
    assert ball.sender.direction == 1

    # move ball to opponent hitting zone
    freezer.skip_to_position(ball, ball.receiver.first_hit_position + 1)

    # opponent sends back again
    with mock_button_press(ball.receiver):
        game.pingpong()
    assert ball.sender.direction == -1

    assert get_pixel_color(game, volley_bonus.coord) == volley_bonus.color
    assert (
        get_range_color(game, volley_bonus.coord + 1, volley_bonus.coord + volley_bonus.length - 1)
        == Colors.BLACK
    )
    assert get_pixel_color(game, volley_bonus.coord + volley_bonus.length - 1) == volley_bonus.color

    # move ball to after bonus
    freezer.skip_to_position(ball, volley_bonus.coord - 1)
    assert volley_bonus not in game.active_bonuses
    assert ball.sender.direction == -1

    assert get_range_color(game, volley_bonus.coord, volley_bonus.coord + volley_bonus.length) == Colors.BLACK
