import time

import network

from ponguy import config
from ponguy.constants import Colors


def do_connect(strip):
    if not config.WIFI_SSID:
        return

    wlan = network.WLAN(network.STA_IF)
    if wlan.isconnected():
        print('network config:', wlan.ifconfig())
        return

    strip.turn_all_off()

    # deactivate interface to avoid Wifi Internal Error from broken previous connection
    wlan.active(False)

    # activate interface
    wlan.active(True)

    # disable power management
    wlan.config(pm=wlan.PM_NONE)

    wlan.connect(config.WIFI_SSID, config.WIFI_PASS)

    timeout = config.WIFI_CONNECTION_TIMEOUT * 1000
    progress_tick = timeout // strip.num_leds

    i = 0
    while not wlan.isconnected():
        time.sleep_ms(progress_tick)
        strip.turn_on(Colors.GREEN, i)
        strip.show()

        if (i := i + 1) >= strip.num_leds:
            print('connection aborted')
            wlan.active(False)

            strip.turn_range_on(Colors.RED, 0, i)
            strip.show()

            time.sleep_ms(1000)
            return

    print('network config:', wlan.ifconfig())

    for j in range(i, strip.num_leds):
        strip.turn_on(Colors.GREEN, j)
        time.sleep_ms(5)
        strip.show()

    time.sleep_ms(500)
