from random import randint

import machine
import micropython
import urequests

from ponguy import config
from ponguy.constants import RAINBOW_COLORS


def get_random_color():
    color_index = randint(0, len(RAINBOW_COLORS) - 1)
    return RAINBOW_COLORS[color_index]


@micropython.native
def fade_to_black(color, amount):
    return tuple((x * int(amount)) >> 8 for x in color)


class FakePin(machine.Pin):
    def __init__(self):
        super().__init__(1)

    def value(self, *args, **kwargs):
        return 1


def send_request(url, payload):
    payload = {'data': payload}
    try:
        resp = urequests.post(url, auth=(config.API_USER, config.API_SECRET), json=payload, timeout=5)
    except ValueError as e:
        print('requests error: %s' % e)
        return

    if resp.status_code != 200:
        print('response status not 200 : %s' % resp.status_code)
        return

    try:
        json_resp = resp.json()
    except ValueError:
        print('response payload is not json')
        return

    if json_resp.get('err', 0) != 0:
        print('response contains error: %s' % json_resp)
        return

    try:
        return json_resp['data']
    except KeyError:
        print('response is missing data key: %s' % json_resp)
        return
