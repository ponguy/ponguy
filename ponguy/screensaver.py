from random import randint

from ponguy import animation


class Screensaver:
    animation_classes = [
        animation.RandomColors,
        animation.AutoGameAnimation,
        animation.RastaFlag,
    ]

    def __init__(self, strip):
        self.strip = strip
        self.animation_classes = [x for x in self.animation_classes if x.probability]

    def start(self):
        total_proba = 0
        for animation_class in self.animation_classes:
            if not animation_class.can_be_added(self):
                continue
            total_proba += animation_class.probability

        proba = randint(0, total_proba)
        incr_proba = 0
        for animation_class in self.animation_classes:
            if not animation_class.can_be_added(self):
                continue
            incr_proba += animation_class.probability
            if proba < incr_proba:
                animation = animation_class(self.strip)
                print('Start screensaver "%s"' % animation_class.__qualname__)
                self.strip.set_animation(animation)
                break

    def animate(self):
        if not self.strip.animate():
            self.start()
