LED_PIN = 23
LED_CLOCK_PIN = 18
NUM_LEDS = 150
START_OFFSET = 0
END_OFFSET = 0

# BRIGHTNESS should be in range [1, 30]
BRIGHTNESS = 1

BLUE_BUTTON_PIN = 17
RED_BUTTON_PIN = 16

# Useful in some setups, for example to power button leds
PINS_TO_POWER = []

BALL_LENGTH = 5
WIN_ANIMATION_SECONDS = 5
ROUND_ANIMATION_SECONDS = 2
DELAY = 5000
POINTS_TO_WIN = 3
HITTING_RANGE = 10
FADING_DELAY = 1.0
FADING_DELAY_MULTIBALL = FADING_DELAY / 3

PERFECT_HIT_BOOST = 2
LATE_HIT_BOOST = 1.5
FIRST_HIT_BOOST = 1.2

MIN_BONUS_DELAY = 2.0
MAX_BONUS_DELAY = 20.0
MAX_BONUS_COUNT = 4

# Probability of game starting with special parameters.
# Possible values:
# * 0 = disabled
# * 1 = always
# * 2 = once every two games
# * n = once every n games
GAME_PRESETS_PROBABILITY = 0

SPEEDING_BONUS_PROBABILITY = 55
SPEEDING_BONUS_LENGTH = 5
SPEEDING_BONUS_INITIAL_BOOST = 0.8
SPEEDING_BONUS_FINAL_BOOST = 2.0
RAINBOW_BONUS_PROBABILITY = 40
RAINBOW_BONUS_LENGTH = 5
BLINKING_BONUS_PROBABILITY = 80
BLINKING_BONUS_LENGTH = 6
LIFE_ADD_BONUS_PROBABILITY = 100
LIFE_ADD_BONUS_LENGTH = 3
BALL_ADD_BONUS_PROBABILITY = 65
BALL_ADD_BONUS_LENGTH = 6
BREAKOUT_BONUS_PROBABILITY = 80
BREAKOUT_BONUS_LENGTH = 5
VOLLEY_BONUS_PROBABILITY = 65
VOLLEY_BONUS_LENGTH = HITTING_RANGE - 2
# reverse bonus is hard, disable by default
REVERSE_BONUS_PROBABILITY = 0
REVERSE_BONUS_LENGTH = 6

RANDOM_COLORS_ANIMATION_PROBABILITY = 10
AUTO_GAME_ANIMATION_PROBABILITY = 1
RASTA_FLAG_ANIMATION_PROBABILITY = 5

# Scoreboard
API_CREATE_URL = None
API_EDIT_URL = None
API_USER = None
API_SECRET = None
API_GAME_SLUG = None

# Wifi
WIFI_SSID = None
WIFI_PASS = None
WIFI_CONNECTION_TIMEOUT = 10
