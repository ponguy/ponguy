import json

from ponguy import default_config

try:
    from ponguy import config_override
except ImportError:
    config_override = None


class SettingsHolder:
    mutable_settings = [
        'START_OFFSET',
        'END_OFFSET',
        'BRIGHTNESS',
        'DELAY',
        'BALL_LENGTH',
        'POINTS_TO_WIN',
        'HITTING_RANGE',
        'MAX_BONUS_DELAY',
        'MIN_BONUS_DELAY',
        'MAX_BONUS_COUNT',
        'PERFECT_HIT_BOOST',
        'LATE_HIT_BOOST',
        'FIRST_HIT_BOOST',
        'GAME_PRESETS_PROBABILITY',
        'SPEEDING_BONUS_PROBABILITY',
        'SPEEDING_BONUS_LENGTH',
        'SPEEDING_BONUS_INITIAL_BOOST',
        'SPEEDING_BONUS_FINAL_BOOST',
        'RAINBOW_BONUS_PROBABILITY',
        'RAINBOW_BONUS_LENGTH',
        'BLINKING_BONUS_PROBABILITY',
        'BLINKING_BONUS_LENGTH',
        'LIFE_ADD_BONUS_PROBABILITY',
        'LIFE_ADD_BONUS_LENGTH',
        'BALL_ADD_BONUS_PROBABILITY',
        'BALL_ADD_BONUS_LENGTH',
        'BREAKOUT_BONUS_PROBABILITY',
        'BREAKOUT_BONUS_LENGTH',
        'VOLLEY_BONUS_PROBABILITY',
        'REVERSE_BONUS_PROBABILITY',
        'REVERSE_BONUS_LENGTH',
    ]

    ball_settings = {
        'BALL_LENGTH': 'length',
        'PERFECT_HIT_BOOST': 'perfect_hit_speed',
        'LATE_HIT_BOOST': 'late_hit_speed',
        'FIRST_HIT_BOOST': 'first_hit_speed',
    }

    def __init__(self):
        settings = default_config.__dict__

        if config_override:
            settings.update(config_override.__dict__)

        for setting, value in settings.items():
            if not setting.startswith('_'):
                setattr(self, setting, value)

        try:
            with open('last_config.json') as f:
                last_config = json.loads(f.read())
        except OSError:  # no FileNotFoundError in micropython
            pass
        except ValueError:
            print('Error: invalid JSON in last_config.json')
        else:
            print('Using config from previous remote control instructions')

            for setting, value in last_config.items():
                setattr(self, setting.upper(), value)

    def to_json(self):
        return {k.lower(): v for k, v in self.__dict__.items() if k in self.mutable_settings}

    def apply_json(self, data):
        for setting, value in data.items():
            setting = setting.upper()

            if setting not in self.mutable_settings:
                continue

            if isinstance(getattr(self, setting), int):
                value = int(value)
            elif isinstance(getattr(self, setting), float):
                value = float(value)

            if getattr(self, setting) == value:
                continue

            setattr(self, setting, value)

            if hasattr(self, 'apply_%s' % setting.lower()):
                getattr(self, 'apply_%s' % setting.lower())(value)

            if setting in self.ball_settings:
                self.apply_ball_setting(self.ball_settings[setting], value)

            if 'BONUS' in setting:
                self.apply_bonus_setting(setting, value)

            if setting in ('START_OFFSET', 'END_OFFSET', 'BRIGHTNESS'):
                self.reinitialise_led_strip()

        with open('last_config.json', 'w') as f:
            f.write(json.dumps(self.to_json()))

    def apply_delay(self, value):
        from ponguy.game import Game

        Game.speed = value

        if game := self.runner.get_current_game():
            game.speed = value

    def apply_points_to_win(self, value):
        from ponguy.player import Player

        Player.life_points = value

    def apply_hitting_range(self, value):
        from ponguy.player import Player

        Player.hitting_range = value

        if game := self.runner.get_current_game():
            for player in game.players:
                player.hitting_range = value
                player.set_first_hit_position()

    def apply_ball_setting(self, setting, value):
        from ponguy.ball import Ball

        setattr(Ball, setting, value)

        if game := self.runner.get_current_game():
            for ball in game.balls:
                setattr(ball, setting, value)

    def reinitialise_led_strip(self):
        self.runner.strip.__init__()

        if game := self.runner.get_current_game():
            game.players[1].position = self.runner.strip.num_leds - 1
            game.players[1].set_first_hit_position()

    def apply_bonus_setting(self, setting, value):
        from ponguy.game import Game

        bonus_name, attribute = setting.lower().rsplit('_', 1)
        bonus_name = ''.join([x[0].upper() + x[1:] for x in bonus_name.split('_')])

        for bonus_class in Game.bonus_classes:
            if bonus_class.__qualname__ == bonus_name:
                setattr(bonus_class, attribute, value)

        if game := self.runner.get_current_game():
            for bonus in game.bonuses:
                if bonus.__class__.__qualname__ == bonus_name:
                    setattr(bonus, attribute, value)
