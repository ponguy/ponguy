import time

import machine

from ponguy import config
from ponguy.button import Button
from ponguy.game import Game
from ponguy.remote_control import RemoteControl
from ponguy.screensaver import Screensaver
from ponguy.strip import Strip
from ponguy.wifi import do_connect


class Runner:
    game = None
    remote_control = None

    def __init__(self, strip=None):
        config.runner = self
        self.strip = strip or Strip()
        self.screensaver = Screensaver(self.strip)
        self.red_button = Button(config.RED_BUTTON_PIN)
        self.blue_button = Button(config.BLUE_BUTTON_PIN)

        for pin in config.PINS_TO_POWER:
            pin = machine.Pin(pin, machine.Pin.OUT)
            pin.on()

    def run(self):
        self.strip.show()

        if self.remote_control:
            self.remote_control.process_pending_requests()

        if not self.game:
            self.maybe_start_game()
            self.screensaver.animate()
        else:
            if self.game.state == self.game.WAITING:
                self.game = None
            else:
                self.game.play()

    def maybe_start_game(self):
        for button in [self.red_button, self.blue_button]:
            if button.check_for_pressing():
                button.first_press_timestamp = time.ticks_ms()
            elif button.first_press_timestamp:
                if time.ticks_diff(time.ticks_ms(), button.first_press_timestamp) > 4000:
                    self.game = self.make_game(button, receiver_is_a_bot=True)
                elif not button.holding():
                    self.game = self.make_game(button)

    def make_game(self, pressed_button, receiver_is_a_bot=False):
        self.blue_button.first_press_timestamp = self.red_button.first_press_timestamp = 0

        return Game(self.strip, self.red_button, self.blue_button, pressed_button, receiver_is_a_bot)

    def get_current_game(self):
        return self.game or getattr(self.strip.animation, 'game', None)

    def connect_to_wifi(self):
        do_connect(self.strip)
        self.remote_control = RemoteControl()
