from random import randint

from ponguy import bonus
from ponguy.animation import RoundPresetAnimation


def get_random_game_preset():
    preset_classes = [
        GamePresetNoBonus,
        GamePresetOnlyMultiBall,
        GamePresetOnlyInversion,
        GamePresetOnlyBreakoutAndBlink,
        GamePresetOnlyBreakoutAndRainbow,
    ]

    return preset_classes[randint(0, len(preset_classes) - 1)]()


class GamePreset:
    def apply(self, game):
        from ponguy.game import Game

        # set special animation on round start
        game.round_animation_class = RoundPresetAnimation

        # double bonus probabilities
        game.get_bonus_delay_us = lambda: Game.get_bonus_delay_us() // 2

        # force bonuses to have same apparition probability
        def get_random_bonus_class():
            valid_bonus_classes = [x for x in game.bonus_classes if x.can_be_added(game)]
            if not valid_bonus_classes:
                return

            i = randint(0, len(valid_bonus_classes) - 1)
            return valid_bonus_classes[i]

        game.get_random_bonus_class = get_random_bonus_class


class GamePresetNoBonus(GamePreset):
    def apply(self, game):
        super().apply(game)
        game.bonus_classes = []
        game.speed = int(game.speed * 0.8)


class GamePresetOnlyMultiBall(GamePreset):
    def apply(self, game):
        super().apply(game)

        for player in game.players:
            player.life_points = 5

        game.bonus_classes = [bonus.BallAddBonus]


class GamePresetOnlyInversion(GamePreset):
    def apply(self, game):
        super().apply(game)

        game.bonus_classes = [bonus.ReverseBonus]


class GamePresetOnlyBreakoutAndBlink(GamePreset):
    def apply(self, game):
        super().apply(game)

        game.bonus_classes = [bonus.BreakoutBonus, bonus.BlinkingBonus]


class GamePresetOnlyBreakoutAndRainbow(GamePreset):
    def apply(self, game):
        super().apply(game)

        game.bonus_classes = [bonus.BreakoutBonus, bonus.RainbowBonus]
