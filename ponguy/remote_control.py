import json
import select
import socket

from ponguy import config


class RemoteControl:
    def __init__(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.setblocking(False)
        s.bind(('0.0.0.0', 80))
        s.listen(1)

        self.poller = select.poll()
        self.poller.register(s, select.POLLIN)

    def process_pending_requests(self):
        results = self.poller.poll(0)
        if not results:
            return

        for socket, _ in results:
            self.process_socket_event(socket)

    def process_socket_event(self, socket):
        cl, _ = socket.accept()
        cl.settimeout(1)

        # receive headers
        request = b''
        while True:
            try:
                request += cl.recv(4096)
            except OSError as e:
                print('Error in receive headers: %s' % e)
                return

            if b'\r\n\r\n' in request:
                break

        headers, body = request.split(b'\r\n\r\n')

        # parse headers
        headers = headers.split(b'\r\n')
        method_header = headers.pop(0)
        headers = dict(header.split(b':', 1) for header in headers)

        if method_header.startswith(b'GET'):
            return self.send_config(cl)

        # receive rest of the body if needed
        content_length = int(headers[b'Content-Length'])
        while len(body) < content_length:
            try:
                body += cl.recv(4096)
            except OSError as e:
                print('Error in receive body: %s' % e)
                return

        return self.update_config(cl, body)

    def send_config(self, cl):
        self.write_response(cl, json.dumps(config.to_json()))

    def update_config(self, cl, body):
        try:
            config_data = json.loads(body)
        except ValueError as e:
            print('JSON error: %s' % e)
        else:
            config.apply_json(config_data)

        self.write_response(cl, '{}')

    @staticmethod
    def write_response(cl, response):
        cl.settimeout(1000)
        cl.write('HTTP/1.0 200 OK\r\n')
        cl.write('Content-type: application/json\r\n')
        cl.write('Access-Control-Allow-Origin: *\r\n')
        cl.write('Connection: close\r\n\r\n')
        cl.write(response)
        cl.close()
