from random import randint

from ponguy import config


class Player:
    life_points = config.POINTS_TO_WIN
    hitting_range = config.HITTING_RANGE
    is_a_bot = False
    pressing = False
    bonus = None

    def __init__(self, button, color, position, direction, is_a_bot=False):
        self.button = button
        self.color = color
        self.position = position
        self.direction = direction
        self.is_a_bot = is_a_bot
        self.set_first_hit_position()

    def set_first_hit_position(self):
        self.first_hit_position = self.position + self.direction * (config.HITTING_RANGE - 1)

    def check_button_press(self):
        self.pressing = self.check_for_pressing()

    def check_for_pressing(self):
        pressing = self.button.check_for_pressing()

        if pressing:
            self.is_a_bot = False

        if self.is_a_bot:
            return True

        return pressing

    def send_back(self, ball):
        if self.bonus and self.bonus.send_back(ball):
            return True

        min_position = self.position if self.direction == 1 else self.first_hit_position
        max_position = self.first_hit_position if self.direction == 1 else self.position
        if not ball.is_in_range(min_position, max_position):
            if (
                self.pressing
                and not self.is_a_bot
                and (not self.bonus or not self.bonus.disable_dead_zone)
                and ball.is_in_range(min_position - self.hitting_range, max_position + self.hitting_range)
            ):
                ball.speed = 0
            return False

        if not self.pressing:
            return False

        if self.is_a_bot:
            ball.set_position(randint(min_position, max_position))

        ball.plain_trail = False
        if ball.position == self.first_hit_position:
            ball.speed = ball.first_hit_speed
        elif ball.position == self.position:
            ball.speed = ball.perfect_hit_speed
            ball.plain_trail = True
        elif self.direction == 1 and ball.position < 3:
            ball.speed = ball.late_hit_speed
        elif self.direction == -1 and ball.position > self.position - 3:
            ball.speed = ball.late_hit_speed
        else:
            ball.speed = 1

        return True

    def get_dot_color(self):
        return self.bonus.get_color() if self.bonus else self.color
