import time

import micropython

from ponguy import Colors, config
from ponguy.constants import RAINBOW_COLORS


class Bonus:
    disable_dead_zone = False

    def __init__(self, game, strip, coord):
        self.game = game
        self.strip = strip
        self.coord = coord
        self.end_coord = coord + self.length - 1

    def can_be_taken(self, ball):
        return True

    def action_on_take(self, ball):
        ball.sender.bonus = self

    def action_on_send_back(self, ball):
        ball.sender.bonus = None
        ball.bonus = self

    def has_action_on_ball_move(self, ball):
        return False

    def action_on_ball_move(self, ball):
        pass

    def send_back(self, ball):
        return False

    def get_color(self):
        return self.color

    @micropython.native
    def display(self):
        self.strip.turn_range_on(self.get_color(), self.coord, self.end_coord)

    def display_ball(self, ball):
        return False

    def get_speed_boost(self):
        return 1

    @classmethod
    def can_be_added(cls, game):
        return len(game.balls) == 1

    @classmethod
    def get_probability(cls, game):
        return cls.probability


class FixedColorBonus(Bonus):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.colors_buffer = self.strip.get_buffer(self.length)
        colors = [self.color for i in range(self.length)]
        self.strip.fill_color_buffer(colors, self.colors_buffer, self.length)

    @micropython.native
    def display(self):
        self.strip.turn_range_on_from_buffer(self.colors_buffer, self.coord, self.end_coord, 0)


class SpeedingBonus(FixedColorBonus):
    probability = config.SPEEDING_BONUS_PROBABILITY
    length = config.SPEEDING_BONUS_LENGTH
    color = Colors.PURPLE

    def action_on_send_back(self, ball):
        super().action_on_send_back(ball)
        self.speed_boost = config.SPEEDING_BONUS_INITIAL_BOOST
        self.speed_increment = (config.SPEEDING_BONUS_FINAL_BOOST - config.SPEEDING_BONUS_INITIAL_BOOST) / (
            self.strip.num_leds - 2 * config.HITTING_RANGE
        )

    def get_speed_boost(self):
        return self.speed_boost

    def has_action_on_ball_move(self, ball):
        return True

    def action_on_ball_move(self, ball):
        if ball.bonus == self:
            self.speed_boost += self.speed_increment


class BlinkingBonus(FixedColorBonus):
    probability = config.BLINKING_BONUS_PROBABILITY
    length = config.BLINKING_BONUS_LENGTH
    color = Colors.YELLOW

    blinking_delay = 150

    def __init__(self, *args):
        super().__init__(*args)
        self.show = True
        self.previous_time = 0

    def can_be_taken(self, ball):
        if ball.bonus:
            return False

        return True

    def action_on_take(self, ball):
        ball.bonus = self

    @micropython.native
    def check_time(self):
        current_time = time.ticks_ms()
        # blink is not even, the ball is shown a bit more than it is hidden
        actual_delay = self.blinking_delay * 1.5 if self.show else self.blinking_delay
        if time.ticks_diff(current_time, self.previous_time) < actual_delay:
            return False
        self.previous_time = current_time
        return True

    @micropython.native
    def get_color(self):
        if self.check_time():
            self.show = not self.show

        if self.show:
            return self.color
        else:
            return Colors.BLACK

    def display(self):
        if self.get_color() == self.color:
            return super().display()

    @classmethod
    def get_probability(cls, game):
        factor = 1
        if any(isinstance(bonus, BreakoutBonus) for bonus in game.bonuses):
            factor = 2
        return factor * cls.probability


class BreakoutBonus(FixedColorBonus):
    probability = config.BREAKOUT_BONUS_PROBABILITY
    length = config.BREAKOUT_BONUS_LENGTH
    color = Colors.LIGHT_WHITE

    blinking_delay = 120000

    def __init__(self, *args):
        super().__init__(*args)
        self.show = True
        self.blocking = False
        self.previous_micros = self.game.current_micros

    @micropython.native
    def check_time(self):
        if time.ticks_diff(self.game.current_micros, self.previous_micros) < self.blinking_delay:
            return False
        self.previous_micros = self.game.current_micros
        return True

    @micropython.native
    def get_color(self):
        if self.blocking:
            return self.color

        if self.check_time():
            self.show = not self.show

        if self.show:
            return self.color
        else:
            return Colors.BLACK

    @micropython.native
    def display(self):
        if self.get_color() != self.color:
            return
        self.strip.turn_range_on_from_buffer(self.colors_buffer, self.coord, self.end_coord, 0)

    def action_on_take(self, ball):
        self.blocking = True
        ball.game.active_bonuses.append(self)

    @micropython.native
    def has_action_on_ball_move(self, ball):
        if not self.blocking:
            return False

        if isinstance(ball.bonus, BlinkingBonus):
            return False

        if ball.sender.direction == 1 and ball.position != self.coord - 1:
            return False

        if ball.sender.direction == -1 and ball.position != self.coord + self.length:
            return False

        return True

    @micropython.native
    def action_on_ball_move(self, ball):
        ball.switch_player()


class LifeAddBonus(FixedColorBonus):
    probability = config.LIFE_ADD_BONUS_PROBABILITY
    length = config.LIFE_ADD_BONUS_LENGTH
    color = Colors.GREEN

    def action_on_take(self, ball):
        if ball.sender.life_points >= 2 * config.POINTS_TO_WIN:
            return

        ball.sender.life_points += 1
        if not ball.bonus:
            ball.bonus = self

    @classmethod
    def can_be_added(cls, game):
        return True


class BallAddBonus(Bonus):
    probability = config.BALL_ADD_BONUS_PROBABILITY
    length = config.BALL_ADD_BONUS_LENGTH
    go_towards_blue = True
    red = 255
    blue = 0

    def action_on_take(self, ball):
        self.game.add_ball(ball.sender)

    @staticmethod
    @micropython.native
    def get_color_transition_increment(pixel):
        if pixel <= 128:
            return pixel / 128 + 0.4
        else:
            return -1 * pixel / 255 + 1.4

    @micropython.native
    def get_color(self):
        increment = self.get_color_transition_increment(self.red)

        if self.go_towards_blue:
            self.red -= increment
            self.blue += increment
        else:
            self.red += increment
            self.blue -= increment

        if self.red <= 0 or self.blue >= 255:
            self.go_towards_blue = False
            self.red = 0
            self.blue = 255
        elif self.red >= 255 or self.blue <= 0:
            self.go_towards_blue = True
            self.red = 255
            self.blue = 0

        return (int(self.red), 0, int(self.blue))

    @classmethod
    def can_be_added(cls, game):
        return len(game.balls) < 3


class RainbowBonus(Bonus):
    probability = config.RAINBOW_BONUS_PROBABILITY
    length = config.RAINBOW_BONUS_LENGTH
    color_index = 0

    disable_dead_zone = True

    def __init__(self, *args):
        super().__init__(*args)
        self.previous_micros = self.game.current_micros

    def can_be_taken(self, ball):
        if ball.sender.bonus:
            return False

        return True

    def action_on_send_back(self, ball):
        super().action_on_send_back(ball)
        try:
            self.color_index = RAINBOW_COLORS.index(ball.sender.color)
        except ValueError:
            self.color_index = 0

    def send_back(self, ball):
        if ball.position == ball.receiver.first_hit_position:
            ball.speed = ball.perfect_hit_speed
            ball.plain_trail = True
            return True
        return False

    @micropython.native
    def get_color(self):
        while self.previous_micros + 2000 < self.game.current_micros:
            self.color_index += 1
            self.previous_micros += 2000

        return RAINBOW_COLORS[self.color_index % len(RAINBOW_COLORS)]

    @micropython.native
    def display_ball(self, ball):
        if ball.sender.direction == 1:
            start = ball.start_position
            end = ball.position
            color_index = self.color_index - start * 7
        else:
            start = ball.position
            end = ball.start_position
            color_index = self.color_index
            color_index = self.color_index - end * 7

        self.strip.turn_range_on_rainbow(start, end, color_index, 7)

        if ball.speed == 0:
            self.strip.fade_range_by(start, end, ball.get_fading_factor())
        return True


class ReverseBonus(Bonus):
    probability = config.REVERSE_BONUS_PROBABILITY
    length = config.REVERSE_BONUS_LENGTH

    blinking_delay = 300000

    def __init__(self, *args):
        super().__init__(*args)
        self.previous_micros = self.game.current_micros
        self.first_color = self.game.players[0].color
        self.second_color = self.game.players[1].color

    def action_on_take(self, ball):
        self.strip.reversed_display = not self.strip.reversed_display

    @micropython.native
    def display(self):
        if self.check_time():
            self.first_color, self.second_color = self.second_color, self.first_color

        self.strip.turn_range_on(self.first_color, self.coord, self.coord + self.length // 2 - 1)
        self.strip.turn_range_on(self.second_color, self.coord + self.length // 2, self.end_coord)

    @micropython.native
    def check_time(self):
        if time.ticks_diff(self.game.current_micros, self.previous_micros) < self.blinking_delay:
            return False
        self.previous_micros = self.game.current_micros
        return True


class VolleyBonus(Bonus):
    probability = config.VOLLEY_BONUS_PROBABILITY
    length = config.VOLLEY_BONUS_LENGTH
    color = Colors.ORANGE

    player = None
    active = False

    def can_be_taken(self, ball):
        if ball.sender.bonus:
            return False

        return True

    def action_on_take(self, ball):
        ball.sender.bonus = self
        self.active = True
        self.player = ball.sender
        self.color = ball.sender.color
        ball.game.active_bonuses.append(self)

    def send_back(self, ball):
        if (
            ball.receiver == self.player
            and self.player.pressing
            and ball.is_in_range(self.coord, self.end_coord)
        ):
            return True
        return False

    def action_on_send_back(self, ball):
        pass

    @micropython.native
    def has_action_on_ball_move(self, ball):
        if not self.active:
            return False

        if ball.receiver != self.player:
            return False

        if self.player.direction > 0 and ball.position < self.coord:
            return True

        if self.player.direction < 0 and ball.position > self.end_coord:
            return True

        return False

    @micropython.native
    def action_on_ball_move(self, ball):
        self.player.bonus = None

    @micropython.native
    def display(self):
        if not self.active:
            self.strip.turn_range_on(self.get_color(), self.coord, self.end_coord)
            self.strip.fade_range_by(self.coord + 1, self.end_coord - 1, 30)
        else:
            self.strip.turn_on(self.get_color(), self.coord)
            self.strip.turn_on(self.get_color(), self.end_coord)

    @classmethod
    def can_be_added(cls, game):
        return (
            Bonus.can_be_added(game) and len([bonus for bonus in game.bonuses if isinstance(bonus, cls)]) < 2
        )
