import math
import time
from random import randint, randrange

from ponguy import Colors, config
from ponguy.constants import RAINBOW_COLORS
from ponguy.utils import FakePin, fade_to_black, get_random_color


class BaseAnimation:
    duriation_ms = 0  # 0 means infinite
    is_done = False
    turn_all_off_when_done = False

    def __init__(self, strip):
        self.strip = strip

    def init_anim(self):
        self.start_time = time.ticks_ms()
        self.previous_time = 0

    def done(self):
        if (
            self.duration_ms
            and time.ticks_diff(time.ticks_ms(), self.start_time) >= self.duration_ms
            or self.is_done
        ):
            if self.turn_all_off_when_done:
                self.strip.turn_all_off()
            return True
        return False

    def check_time(self, ms_delay):
        current_time = time.ticks_ms()
        if time.ticks_diff(current_time, self.previous_time) < ms_delay:
            return False
        self.previous_time = current_time
        return True

    @classmethod
    def can_be_added(cls, screensaver):
        return True


class RandomColors(BaseAnimation):
    probability = config.RANDOM_COLORS_ANIMATION_PROBABILITY

    def __init__(self, strip):
        super().__init__(strip)
        self.duration_ms = 60000

    def play(self):
        i = randint(0, self.strip.num_leds - 1)
        self.strip.turn_on(get_random_color(), i)


class AutoGameAnimation(BaseAnimation):
    probability = config.AUTO_GAME_ANIMATION_PROBABILITY

    def __init__(self, strip):
        from ponguy.main import Runner

        super().__init__(strip)
        self.duration_ms = 30000

        runner = Runner(self.strip)
        runner.red_button.pin = runner.blue_button.pin = FakePin()
        pressed_button = runner.red_button if not randint(0, 1) else runner.blue_button

        self.game = runner.make_game(pressed_button)

        p1_color_idx = randrange(len(RAINBOW_COLORS))
        p2_color_idx = int(p1_color_idx + len(RAINBOW_COLORS) / 2) % len(RAINBOW_COLORS)
        colors = (RAINBOW_COLORS[p1_color_idx], RAINBOW_COLORS[p2_color_idx])

        for player, color in zip(self.game.players, colors):
            player.color = color
            player.is_a_bot = True

        self.game.start_pingpong()

    def play(self):
        self.game.play()

    @classmethod
    def can_be_added(cls, screensaver):
        is_only_animation = bool(screensaver.animation_classes == [cls])
        is_previous_animation = isinstance(screensaver.strip.previous_animation, cls)
        follow_win_animation = isinstance(screensaver.strip.previous_animation, WinAnimation)
        return is_only_animation or not (is_previous_animation or follow_win_animation)


class RastaFlag(BaseAnimation):
    """
    Display a Rasta flag made up of green, yellow and red colors.
    The flag is animated with waves that move in time.
    """

    probability = config.RASTA_FLAG_ANIMATION_PROBABILITY
    # Number of visible waves
    waves_count = 4
    # Min intensity threshold (0-255)
    min_lum = 10
    # Group displayed pixels by range for faster waves
    color_groups = 5

    def __init__(self, strip):
        super().__init__(strip)
        self.duration_ms = 60000
        self._offset = 0

    def play(self):
        for i in range(0, self.strip.num_leds, self.color_groups):
            color = Colors.GREEN
            if i > self.strip.num_leds / 3:
                # Orange renders better than the yellow on the rasta flag
                color = Colors.ORANGE
            if i > self.strip.num_leds * 2 / 3:
                color = Colors.RED
            fading_percent = (
                math.cos((i + self._offset) * self.waves_count * math.pi / self.strip.num_leds) ** 2
            )
            color = fade_to_black(color, fading_percent * (256 - self.min_lum) + self.min_lum)
            self.strip.turn_range_on(color, i, min(i + self.color_groups, self.strip.num_leds - 1))
        # Move forward
        self._offset -= 1


class BaseGameAnimation(BaseAnimation):
    turn_all_off_when_done = True

    def build_score_bar(self):
        self.score_bar = []
        for i in range(3):
            self.score_bar.append((0, 0, 0))

        score_length = max(config.POINTS_TO_WIN, self.game.players[0].life_points)
        for i in range(score_length):
            if i < self.game.players[0].life_points:
                self.score_bar.append(self.game.players[0].color)
            else:
                self.score_bar.append((0, 0, 0))

        score_length = max(config.POINTS_TO_WIN, self.game.players[1].life_points)
        for i in range(score_length):
            if i >= (score_length - self.game.players[1].life_points):
                self.score_bar.append(self.game.players[1].color)
            else:
                self.score_bar.append((0, 0, 0))

        for i in range(1, 4):
            self.score_bar.append((0, 0, 0))

    def display_score_bar(self):
        middle = self.strip.num_leds // 2
        start = middle - ((len(self.score_bar) + 1) // 2)
        for i in range(start, start + len(self.score_bar)):
            self.strip.turn_on(self.score_bar[i - start], i)


class WinAnimation(BaseGameAnimation):
    spacing = 4

    def __init__(self, strip, game, player):
        super().__init__(strip)
        self.game = game
        self.player = player
        self.duration_ms = 5000
        self.start = 0
        self.build_score_bar()

    def play(self):
        if not self.check_time(80):
            return

        for i in range(self.start, self.strip.num_leds - 1, self.spacing):
            self.strip.turn_off(i)

        self.start = (self.start - self.player.direction + self.spacing) % self.spacing

        for i in range(self.start, self.strip.num_leds - 1, self.spacing):
            self.strip.turn_on(self.player.color, i)

        self.display_score_bar()


class RoundAnimation(BaseGameAnimation):
    def __init__(self, strip, game, player, first_round=False):
        super().__init__(strip)
        self.game = game
        self.player = player
        self.duration_ms = 2500 if not first_round else 1500
        self.show = False
        self.build_score_bar()

    def play(self):
        start = 0
        end = self.strip.num_leds - 1
        middle = self.strip.num_leds // 2
        if self.player.direction > 0:
            end = middle + 1
        else:
            start = middle
        if not self.check_time(400 / (max(config.POINTS_TO_WIN - self.player.life_points, 1))):
            return
        if self.show:
            self.strip.turn_all_off()
        else:
            self.strip.turn_range_on(self.player.color, start, end)
        self.show = not self.show
        self.display_score_bar()


class RoundPresetAnimation(RoundAnimation):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.middle = self.strip.num_leds // 2
        self.min_leds = self.middle // 4
        self.max_leds = self.middle // 2
        self.increment = 0
        self.increment_direction = 1

    def play(self):
        if not self.check_time(10 // (max(config.POINTS_TO_WIN - self.player.life_points, 1))):
            return

        self.increment += self.increment_direction
        if self.increment in (self.max_leds, 0):
            self.increment_direction *= -1
            self.increment += self.increment_direction

        self.strip.turn_all_off()
        self.strip.turn_range_on(self.player.color, 0, self.min_leds + self.increment)
        if self.player.direction == -1:
            self.strip.reverse_buffer()

        self.display_score_bar()
