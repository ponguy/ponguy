import machine


class Button:
    state = 0
    first_press_timestamp = 0

    def __init__(self, pin_no):
        self.pin = machine.Pin(pin_no, machine.Pin.IN, machine.Pin.PULL_UP)

    def holding(self):
        return bool(self.pin.value() == 0)

    def check_for_pressing(self):
        value = self.pin.value()

        if self.state == value:
            return False

        self.state = value
        return bool(value == 0)
