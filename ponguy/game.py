import _thread
import gc
import time
from random import randint

from ponguy import Colors, bonus, config
from ponguy.animation import RoundAnimation, WinAnimation
from ponguy.ball import Ball
from ponguy.player import Player
from ponguy.preset import get_random_game_preset
from ponguy.utils import fade_to_black, send_request


class Game:
    WAITING = 0
    GETTING_READY = 1
    PLAYING = 2
    ENDING = 3

    speed = config.DELAY
    first_ball_index = 0
    state = WAITING
    round_animation_class = RoundAnimation

    ball_count = 0
    remote_game_id = None
    first_ball_idx = 0

    bonus_classes = [
        bonus.SpeedingBonus,
        bonus.BlinkingBonus,
        bonus.BreakoutBonus,
        bonus.RainbowBonus,
        bonus.BallAddBonus,
        bonus.LifeAddBonus,
        bonus.ReverseBonus,
        bonus.VolleyBonus,
    ]
    max_bonus_length = max(b.length for b in bonus_classes)

    def __init__(
        self, strip, red_button, blue_button, pressed_button, receiver_is_a_bot=False, set_animation=True
    ):
        self.strip = strip
        self.displayed_bonuses = []
        self.active_bonuses = []
        self.balls = []

        self.players = [
            Player(red_button, Colors.RED, 0, 1),
            Player(blue_button, Colors.BLUE, strip.num_leds - 1, -1),
        ]

        if red_button == pressed_button:
            self.sender, self.receiver = self.players[0], self.players[1]
        else:
            self.sender, self.receiver = self.players[1], self.players[0]

        if receiver_is_a_bot:
            self.receiver.is_a_bot = True

        self.players[0].opponent, self.players[1].opponent = self.players[1], self.players[0]

        if config.GAME_PRESETS_PROBABILITY and randint(1, config.GAME_PRESETS_PROBABILITY) == 1:
            preset = get_random_game_preset()
            preset.apply(self)

        self.start_new_round(self.sender, True, set_animation)

    @property
    def bonuses(self):
        for bonuses in (self.displayed_bonuses, self.active_bonuses):
            yield from bonuses

    def play(self):
        if self.state == self.GETTING_READY:
            if not self.strip.animate():
                self.start_pingpong()
        elif self.state == self.PLAYING:
            self.pingpong()
        elif self.state == self.ENDING:
            if not self.strip.animate():
                self.state = self.WAITING

    def pingpong(self):
        self.current_micros = time.ticks_us()

        self.strip.turn_all_off()

        for player in self.players:
            player.check_button_press()

        self.display_hitting_zone_dot()

        self.first_ball_idx = (self.first_ball_idx + 1) % len(self.balls)
        for ball in self.balls[self.first_ball_idx :] + self.balls[: self.first_ball_idx]:
            ball.display()

            if ball.sender.pressing:
                self.check_for_bonus(ball)

            if ball.receiver.send_back(ball):
                self.ball_count += 1
                self.send_back(ball)

            hitted_player = ball.move()
            if hitted_player:
                self.remove_ball(ball)
                hitted_player.life_points -= 1
                if hitted_player.life_points == 0:
                    return self.end_game(hitted_player.opponent)
                elif len(self.balls) == 0:
                    return self.start_new_round(hitted_player.opponent)

        self.add_scheduled_bonus()
        self.display_bonus()

    def send_back(self, ball):
        ball.bonus = None

        ball.switch_player()

        if ball.sender.bonus:
            ball.sender.bonus.action_on_send_back(ball)

        if ball.speed == ball.first_hit_speed:  # sneaky attack
            ball.set_position(round(self.strip.num_leds / 2))

    def remove_ball(self, ball):
        self.balls.remove(ball)

    def start_new_round(self, sender, first_round=False, set_animation=True):
        gc.collect()  # force MicroPython garbage collection
        self.displayed_bonuses.clear()
        self.active_bonuses.clear()

        for player in self.players:
            player.bonus = None

        self.sender = sender

        self.strip.reversed_display = False
        self.strip.turn_all_off()

        if set_animation:
            self.strip.set_animation(self.round_animation_class(self.strip, self, sender, first_round))

        has_bots = any(x.is_a_bot for x in self.players)
        if first_round and config.API_CREATE_URL and not has_bots:
            _thread.start_new_thread(self.api_notify_game_start, ())

        self.state = self.GETTING_READY

    def start_pingpong(self):
        self.scheduled_bonus_micros = time.ticks_add(time.ticks_us(), self.get_bonus_delay_us())
        self.add_ball(self.sender)
        self.state = self.PLAYING

    def add_ball(self, player):
        self.balls.append(Ball(self, self.strip, player))

    def end_game(self, winner):
        self.strip.reversed_display = False
        self.strip.turn_all_off()

        self.strip.set_animation(WinAnimation(self.strip, self, winner))
        self.state = self.ENDING

        if self.remote_game_id:
            _thread.start_new_thread(
                self.api_notify_game_end,
                (winner, self.remote_game_id, self.ball_count, self.game_start_timestamp),
            )

    def display_hitting_zone_dot(self):
        for player in self.players:
            # Fill a bot hitting zone with its faded color
            if player.is_a_bot:
                color = fade_to_black(player.get_dot_color(), 30)
                if player.direction == 1:
                    start = player.position
                    end = player.first_hit_position
                else:
                    start = player.first_hit_position
                    end = player.position

                self.strip.turn_range_on(color, start, end)

            self.strip.turn_on(player.get_dot_color(), player.first_hit_position)
            self.strip.turn_on(player.get_dot_color(), player.position)

    def display_bonus(self):
        for bonus in self.bonuses:
            bonus.display()

    @staticmethod
    def get_bonus_delay_us():
        min_bonus_delay = 1000000 * int(config.MIN_BONUS_DELAY)
        max_bonus_delay = 1000000 * int(config.MAX_BONUS_DELAY)
        return min_bonus_delay + randint(0, max_bonus_delay - min_bonus_delay - 1)

    def get_random_bonus_class(self):
        total_proba = 0
        for bonus_class in self.bonus_classes:
            if not bonus_class.can_be_added(self):
                continue
            total_proba += bonus_class.get_probability(self)

        if not total_proba:
            return

        proba = randint(0, total_proba - 1)
        incr_proba = 0
        for bonus_class in self.bonus_classes:
            if not bonus_class.can_be_added(self):
                continue
            incr_proba += bonus_class.get_probability(self)
            if proba < incr_proba:
                return bonus_class

    def get_random_bonus_coord(self, bonus_class):
        bonuses = sorted(self.bonuses, key=lambda b: b.coord)
        gaps_begin_coords = [self.players[0].hitting_range * 3 - 1] + [
            b.coord + b.length - 1 for b in bonuses
        ]
        gaps_end_coords = [b.coord for b in bonuses] + [
            self.strip.num_leds - self.players[1].hitting_range * 3 - 1
        ]

        available_positions = 0
        for a, b in zip(gaps_begin_coords, gaps_end_coords):
            gap_length = b - (a + 1) - 2
            if gap_length < self.max_bonus_length:
                continue
            available_positions += gap_length - bonus_class.length + 1
        if available_positions == 0:
            return

        position = randint(0, available_positions - 1)

        for a, b in zip(gaps_begin_coords, gaps_end_coords):
            gap_length = b - (a + 1) - 2
            if gap_length < self.max_bonus_length:
                continue
            available_positions = gap_length - bonus_class.length
            if position <= available_positions:
                break
            position -= available_positions
        return a + 1 + position + 1

    def add_scheduled_bonus(self):
        if self.current_micros < self.scheduled_bonus_micros:
            return

        if len(self.displayed_bonuses) + len(self.active_bonuses) >= config.MAX_BONUS_COUNT:
            return

        bonus_class = self.get_random_bonus_class()
        if bonus_class is None:  # no bonuses can be added
            return
        coord = self.get_random_bonus_coord(bonus_class)
        if coord is None:  # no space for more bonuses
            return

        bonus = bonus_class(self, self.strip, coord)

        self.displayed_bonuses.append(bonus)

        self.scheduled_bonus_micros = self.current_micros + self.get_bonus_delay_us()

    def check_for_bonus(self, ball):
        for bonus in reversed(self.displayed_bonuses):
            if ball.is_in_range(bonus.coord, bonus.end_coord) and bonus.can_be_taken(ball):
                bonus.action_on_take(ball)
                self.displayed_bonuses.remove(bonus)

    def api_notify_game_start(self):
        self.game_start_timestamp = time.ticks_us()

        payload = {}
        if config.API_GAME_SLUG:
            payload['game_slug'] = config.API_GAME_SLUG

        json_resp = send_request(config.API_CREATE_URL, payload)
        self.remote_game_id = json_resp.get('id') if json_resp else None

    def api_notify_game_end(self, winner, remote_game_id, ball_count, game_start_timestamp):
        payload = {
            'duration': int(time.ticks_diff(time.ticks_us(), game_start_timestamp) / 10**6),
            'win_color': 'red' if winner.color == Colors.RED else 'blue',
            'ball_count': ball_count,
        }

        send_request(config.API_EDIT_URL % remote_game_id, payload)
