# Ponguy, un pong à base de guirlande LED

Ce projet est un fork MicroPython de https://git.inpt.fr/Zil0/pongui.

Il fonctionne avec une carte ESP32 et une guirlande SK9822.

### Pourquoi ce fork

Le développement en C est fastidieux, notamment parce qu'on ne peut pas écrire de test.

Ce fork embarque une batterie de tests, exécutables localement avec pytest, grâce à des mocks.

## Installation

Les instruction se trouvent sur cette [page wiki](https://gitlab.com/ponguy/ponguy/-/wikis/home).

## Démo

Partie sur une guirlande de 5m, avec des bonus multi-ball :

![](https://www.bde.enseeiht.fr/~rigalv/PONGUY_2022_05.mp4).

## Contribuer

Les contributions sont les bienvenues, elles peuvent concerner une issue existante ou une idée originale.

Il est fortement recommandé d'écrire un test (les tests s'exécutent en lançant simplement pytest à la racine du projet).
